package frontend

import (
	"net/http"
	"os"
	"path"
	"strings"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	p := path.Join("frontend/public", strings.TrimLeft(r.URL.Path, "/"))
	info, err := os.Stat(p)
	if err != nil || info.IsDir() {
		_, err = os.Stat("frontend/public/index.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		p = "frontend/public/index.html"
	}
	http.ServeFile(w, r, p)
}
