package master

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/windows/settings"
)

func NewWindow(app fyne.App, startCallback func(url string)) fyne.Window {
	w := app.NewWindow("F&F Chat Widget")
	w.SetMaster()

	openSettingsButton := widget.NewButton("open settings", func() {
		settings.NewWindow(app).Show()
	})

	urlInput := widget.NewEntry()
	urlInput.SetPlaceHolder("Amazon URL")

	var startButton *widget.Button
	startButton = widget.NewButton("start widget", func() {
		startButton.Disable()
		startCallback(urlInput.Text)
	})

	w.SetContent(container.New(layout.NewVBoxLayout(),
		openSettingsButton,
		container.New(layout.NewBorderLayout(nil, nil, nil, startButton), startButton, urlInput),
	))

	currentSize := w.Content().Size()
	w.Resize(fyne.Size{
		Width:  currentSize.Width * 3,
		Height: currentSize.Height,
	})

	return w
}
