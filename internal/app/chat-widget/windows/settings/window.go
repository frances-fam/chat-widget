package settings

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func NewWindow(app fyne.App) fyne.Window {
	w := app.NewWindow("F&F Chat Widget Settings")

	w.SetContent(container.New(layout.NewVBoxLayout(),
		newForm(app),
		container.New(layout.NewCenterLayout(), widget.NewLabel("The application needs to restart for changes to take effect.")),
	))

	return w
}
