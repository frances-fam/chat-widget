package settings

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

func newForm(app fyne.App) *widget.Form {
	serverAddr := widget.NewEntry()
	serverAddr.SetText(app.Preferences().StringWithFallback("server.addr", ":8450"))
	loggerTimeFormat := widget.NewEntry()
	loggerTimeFormat.SetText(app.Preferences().StringWithFallback("logger.timeFormat", "2006.01.02 15:04:05"))
	loggerLevel := widget.NewEntry()
	loggerLevel.SetText(app.Preferences().StringWithFallback("logger.level", "info"))
	loggerDir := widget.NewEntry()
	loggerDir.SetText(app.Preferences().StringWithFallback("logger.dir", "logs"))
	discordBotToken := widget.NewPasswordEntry()
	discordBotToken.SetText(app.Preferences().String("discord.botToken"))
	discordThreadChannelID := widget.NewEntry()
	discordThreadChannelID.SetText(app.Preferences().String("discord.threadChannelID"))

	return &widget.Form{
		Items: []*widget.FormItem{
			{Text: "server address", Widget: serverAddr},
			{Text: "logger time format", Widget: loggerTimeFormat},
			{Text: "logger level", Widget: loggerLevel},
			{Text: "logger directory", Widget: loggerDir},
			{Text: "Discord bot token", Widget: discordBotToken},
			{Text: "Discord thread channel id", Widget: discordThreadChannelID},
		},
		SubmitText: "Apply",
		OnSubmit: func() {
			app.Preferences().SetString("server.addr", serverAddr.Text)
			app.Preferences().SetString("logger.timeFormat", loggerTimeFormat.Text)
			app.Preferences().SetString("logger.level", loggerLevel.Text)
			app.Preferences().SetString("logger.dir", loggerDir.Text)
			app.Preferences().SetString("discord.botToken", discordBotToken.Text)
			app.Preferences().SetString("discord.threadChannelID", discordThreadChannelID.Text)
		},
	}
}
