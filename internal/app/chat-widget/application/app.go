package application

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"github.com/kataras/golog"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/windows/master"
	"gitlab.com/frances-fam/chat-widget/pkg/show"
)

type App struct {
	Fyne   fyne.App
	Logger *golog.Logger
}

func New() *App {
	a := &App{
		Fyne: app.NewWithID("frances-fam.chat-widget"),
	}

	a.addLogger()

	return a
}

func (a *App) Run() {
	var prov *masterProvider
	var err error
	master.NewWindow(a.Fyne, func(url string) {
		prov, err = a.newMasterProvider(show.New(url))
		if err != nil {
			a.Logger.Fatal(errors.Wrap(err, "new master provider"))
		}
		srv, err := a.newServer(prov)
		if err != nil {
			a.Logger.Fatal(errors.Wrap(err, "new server"))
		}
		go func() {
			if err := srv.ListenAndServe(); err != nil {
				a.Logger.Fatal(errors.Wrap(err, "listen and serve"))
			}
		}()
	}).ShowAndRun()
	if prov != nil {
		prov.Close()
	}
}
