package application

import (
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/server"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
)

func (a *App) newServer(prov provider.Provider) (*server.Server, error) {
	return server.NewServer(a.Fyne.Preferences(), prov)
}
