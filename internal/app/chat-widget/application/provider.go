package application

import (
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/chat/amazon"
	"gitlab.com/frances-fam/chat-widget/pkg/chat/discord"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
	"gitlab.com/frances-fam/chat-widget/pkg/show"
)

type masterProvider struct {
	*provider.Aggregator
	discordChat *discord.Chat
	amazonChat  *amazon.Chat
}

func (a *App) newMasterProvider(s *show.Show) (*masterProvider, error) {
	p := &masterProvider{
		Aggregator: provider.NewAggregator(),
	}
	discordChat, err := discord.NewChat(a.Fyne.Preferences(), s)
	if err != nil {
		return nil, errors.Wrap(err, "new discord chat")
	}
	p.discordChat = discordChat
	if err := p.AddProvider(discordChat.Provider()); err != nil {
		return nil, errors.Wrap(err, "add discord chat provider")
	}

	amazonChat, err := amazon.NewChat(s)
	if err != nil {
		return nil, errors.Wrap(err, "new amazon chat")
	}
	if err := p.AddProvider(amazonChat.Provider()); err != nil {
		return nil, errors.Wrap(err, "add amazon chat provider")
	}

	return p, nil
}

func (p *masterProvider) Close() {
	_ = p.discordChat.Close()
	_ = p.amazonChat.Close()
}
