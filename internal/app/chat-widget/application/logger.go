package application

import "gitlab.com/frances-fam/chat-widget/internal/pkg/logger"

func (a *App) addLogger() {
	l, err := logger.New(a.Fyne)
	if err != nil {
		panic(err)
	}

	a.Logger = l
}
