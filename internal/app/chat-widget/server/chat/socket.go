package chat

import (
	"net/http"
)

func (chat *Chat) SocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := chat.wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}
	l := chat.addNewListener()

	for chat.hasListener(l) {
		m := l.Get()
		go func() {
			m := m
			if err := conn.WriteJSON(m); err != nil {
				chat.removeListener(l)
				_ = conn.Close()
			}
		}()
	}
}
