package chat

import (
	"sync"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/server/upgrader"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
)

type Chat struct {
	mu         sync.RWMutex
	wsUpgrader websocket.Upgrader
	msgChan    <-chan *message.Message
	listeners  map[uuid.UUID]*listener
}

func New(prov provider.Provider) (*Chat, error) {
	c, err := prov.Subscribe()
	if err != nil {
		return nil, errors.Wrap(err, "subscribe to message provider")
	}

	chat := &Chat{
		wsUpgrader: upgrader.New(),
		msgChan:    c,
		listeners:  map[uuid.UUID]*listener{},
	}

	chat.startListening()

	return chat, nil
}
