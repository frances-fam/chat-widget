package chat

import (
	"github.com/google/uuid"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

type listener struct {
	id      uuid.UUID
	channel chan *message.Message
}

func (chat *Chat) startListening() {
	go func() {
		for {
			chat.notifyListeners(<-chat.msgChan)
		}
	}()
}

func (chat *Chat) notifyListeners(m *message.Message) {
	chat.mu.RLock()
	defer chat.mu.RUnlock()
	for _, l := range chat.listeners {
		l.channel <- m
	}
}

func (chat *Chat) hasListener(l *listener) bool {
	chat.mu.RLock()
	defer chat.mu.RUnlock()
	_, ok := chat.listeners[l.id]

	return ok
}

func (chat *Chat) removeListener(l *listener) {
	chat.mu.Lock()
	defer chat.mu.Unlock()
	delete(chat.listeners, l.id)
}

func (chat *Chat) addNewListener() *listener {
	l := newListener()
	chat.addListener(l)

	return l
}

func (chat *Chat) addListener(l *listener) {
	chat.mu.Lock()
	defer chat.mu.Unlock()
	chat.listeners[l.id] = l
}

func newListener() *listener {
	return &listener{
		id:      uuid.New(),
		channel: make(chan *message.Message),
	}
}

func (l *listener) Get() *message.Message {
	return <-l.channel
}
