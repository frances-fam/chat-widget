package server

import (
	"net/http"

	"fyne.io/fyne/v2"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/server/chat"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
)

type Server struct {
	pref   fyne.Preferences
	router *mux.Router
	prov   provider.Provider
	chat   *chat.Chat
}

func NewServer(pref fyne.Preferences, prov provider.Provider) (*Server, error) {
	s := &Server{
		pref:   pref,
		router: mux.NewRouter(),
		prov:   prov,
	}

	if err := s.addChat(); err != nil {
		return nil, errors.Wrap(err, "add chat")
	}

	s.setupRoutes()

	return s, nil
}

func (s *Server) ListenAndServe() error {
	return http.ListenAndServe(s.pref.StringWithFallback("server.addr", ":8450"), s.router)
}
