package upgrader

import (
	"net/http"

	"github.com/gorilla/websocket"
)

func New() websocket.Upgrader {
	return websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
}
