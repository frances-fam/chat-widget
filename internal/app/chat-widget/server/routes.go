package server

import "gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/frontend"

func (s *Server) setupRoutes() {
	s.router.HandleFunc("/chat/ws", s.chat.SocketHandler)
	s.router.PathPrefix("/").HandlerFunc(frontend.Handler)
}
