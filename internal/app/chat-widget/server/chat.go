package server

import (
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/server/chat"
)

func (s *Server) addChat() error {
	c, err := chat.New(s.prov)
	if err != nil {
		return errors.Wrap(err, "new chat")
	}
	s.chat = c

	return nil
}
