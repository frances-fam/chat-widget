package logger

import (
	"fmt"
	"os"
	"path"
	"time"

	"fyne.io/fyne/v2"
	"github.com/kataras/golog"
	"github.com/pkg/errors"
)

func New(app fyne.App) (*golog.Logger, error) {
	logger := golog.New()
	logger.SetPrefix(fmt.Sprintf("[%s] ", app.UniqueID()))
	logger.SetTimeFormat(app.Preferences().StringWithFallback("logger.timeFormat", "2006.01.02 15:04:05"))
	logger.SetLevel(app.Preferences().StringWithFallback("logger.level", "info"))

	f, err := newLogFile(app)
	if err != nil {
		return nil, errors.Wrap(err, "new log file")
	}
	logger.AddOutput(f)

	return logger, nil
}

func newLogFile(app fyne.App) (*os.File, error) {
	dir := app.Preferences().StringWithFallback("logger.dir", "logs")
	if err := os.MkdirAll(dir, 0775); err != nil {
		return nil, errors.Wrap(err, "create log dir")
	}
	now := time.Now()
	fileName := fmt.Sprintf("%x.%s.log", now.UnixNano(), now.Format(time.RFC822Z))
	f, err := os.OpenFile(path.Join(dir, fileName), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0664)
	if err != nil {
		return nil, errors.Wrap(err, "create log file")
	}

	return f, nil
}
