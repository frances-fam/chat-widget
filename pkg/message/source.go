package message

type Source struct {
	ID        string
	Name      string
	AvatarURL string
}
