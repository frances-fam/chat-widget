package message

import "time"

type Message struct {
	ID        string
	Source    *Source
	Sender    *Sender
	Timestamp time.Time
	Payload   interface{}
}
