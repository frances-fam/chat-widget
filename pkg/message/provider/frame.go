package provider

import (
	"sync"

	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

type Frame struct {
	mu               sync.Mutex
	subscriptions    []chan<- *message.Message
	subscribeHandler func() (chan *message.Message, error)
}

func (f *Frame) Init() {
	f.SetSubscribeHandler(func() (chan *message.Message, error) {
		return make(chan *message.Message), nil
	})
}

func (f *Frame) SetSubscribeHandler(subscribeHandler func() (chan *message.Message, error)) {
	f.subscribeHandler = subscribeHandler
}

func (f *Frame) Subscribe() (<-chan *message.Message, error) {
	f.mu.Lock()
	defer f.mu.Unlock()
	c, err := f.subscribeHandler()
	if err != nil {
		return nil, err
	}

	f.subscriptions = append(f.subscriptions, c)

	return c, nil
}

func (f *Frame) Notify(msg *message.Message) {
	f.mu.Lock()
	defer f.mu.Unlock()
	for _, subscription := range f.subscriptions {
		subscription <- msg
	}
}
