package provider

import (
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

type Aggregator struct {
	Frame
}

func NewAggregator() *Aggregator {
	a := &Aggregator{}
	a.Init()

	return a
}

func (a *Aggregator) AddProvider(p Provider) error {
	s, err := p.Subscribe()
	if err != nil {
		return errors.Wrap(err, "subscribe to message provider")
	}

	a.listen(s)

	return nil
}

func (a *Aggregator) listen(msgs <-chan *message.Message) {
	go func() {
		for {
			a.Notify(<-msgs)
		}
	}()
}
