package provider

import (
	"time"

	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

type SingleSource struct {
	Frame
	source *message.Source
}

func NewSingleSource(source *message.Source) *SingleSource {
	s := &SingleSource{
		source: source,
	}
	s.Init()

	return s
}

func (s *SingleSource) AddMessage(sender *message.Sender, timestamp time.Time, payload interface{}) {
	s.Notify(&message.Message{
		Source:    s.source,
		Sender:    sender,
		Timestamp: timestamp,
		Payload:   payload,
	})
}
