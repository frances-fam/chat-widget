package provider

import "gitlab.com/frances-fam/chat-widget/pkg/message"

type Provider interface {
	Subscribe() (<-chan *message.Message, error)
}
