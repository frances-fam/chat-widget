package message

type Sender struct {
	ID        string
	Name      string
	AvatarURL string
}
