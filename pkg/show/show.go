package show

type Show struct {
	URL string
}

func New(url string) *Show {
	return &Show{
		URL: url,
	}
}
