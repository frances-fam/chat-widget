package discord

import (
	"fyne.io/fyne/v2"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
	"gitlab.com/frances-fam/chat-widget/pkg/show"
)

type Chat struct {
	dg            *discordgo.Session
	prov          *provider.SingleSource
	pref          fyne.Preferences
	show          *show.Show
	threadMessage *discordgo.Message
	thread        *discordgo.Channel
}

func NewChat(pref fyne.Preferences, show *show.Show) (*Chat, error) {
	dg, err := discordgo.New("Bot " + pref.String("discord.botToken"))
	if err != nil {
		return nil, errors.Wrap(err, "create new Discord session")
	}

	c := &Chat{
		dg: dg,
		prov: provider.NewSingleSource(&message.Source{
			ID:        "discord",
			Name:      "Discord",
			AvatarURL: "/avatars/discord.png",
		}),
		pref: pref,
		show: show,
	}
	if err := c.startListening(); err != nil {
		return nil, errors.Wrap(err, "start listening")
	}

	return c, nil
}

func (c *Chat) Close() error {
	_, _ = c.dg.ChannelMessageEdit(c.threadMessage.ChannelID, c.threadMessage.ID, c.threadMessageContentInactive())
	_, _ = c.dg.ChannelEditComplex(c.thread.ID, &discordgo.ChannelEdit{Archived: true, Locked: true})

	return errors.Wrap(c.dg.Close(), "close Discord session")
}

func (c *Chat) Provider() provider.Provider {
	return c.prov
}
