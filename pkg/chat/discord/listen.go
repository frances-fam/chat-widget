package discord

import (
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

func (c *Chat) startListening() error {
	channel, err := c.createThread()
	if err != nil {
		return errors.Wrap(err, "create thread")
	}
	c.dg.Identify.Intents = discordgo.IntentsGuildMessages
	if err := c.dg.Open(); err != nil {
		return errors.Wrap(err, "open websocket connection")
	}
	c.dg.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.ChannelID != channel.ID || m.Author.ID == s.State.User.ID {
			return
		}
		c.prov.AddMessage(&message.Sender{
			ID:        "discord" + m.Author.ID,
			Name:      m.Author.Username,
			AvatarURL: m.Author.AvatarURL("256"),
		}, m.Timestamp, m.Content)
	})

	return nil
}
