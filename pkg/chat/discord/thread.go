package discord

import (
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func (c *Chat) createThread() (*discordgo.Channel, error) {
	tlMsg, err := c.dg.ChannelMessageSend(c.pref.String("discord.threadChannelID"), c.threadMessageContentActive())
	if err != nil {
		return nil, errors.Wrap(err, "send channel message")
	}
	c.threadMessage = tlMsg
	channel, err := c.dg.MessageThreadStart(c.pref.String("discord.threadChannelID"), tlMsg.ID, "Live interaction ", 1440)
	if err != nil {
		return nil, errors.Wrap(err, "create interaction thread")
	}
	c.thread = channel
	_, err = c.dg.ChannelMessageSend(channel.ID, c.threadWarningMessageContent())
	if err != nil {
		return nil, errors.Wrap(err, "create thread warning message")
	}

	return channel, nil
}

func (c *Chat) threadMessageContentActive() string {
	return "Frances and Family are live on Amazon :red_circle:\n" + c.show.URL
}

func (c *Chat) threadMessageContentInactive() string {
	return "Frances and Family were live on Amazon :white_circle:"
}

func (c *Chat) threadWarningMessageContent() string {
	return `:warning: Everything posted in this thread may be shown on Amazon Live :warning:`
}
