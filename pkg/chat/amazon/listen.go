package amazon

import (
	"net/url"
	"time"

	"github.com/go-rod/rod"
	"github.com/go-rod/rod/lib/proto"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
)

func (c *Chat) startListening() error {
	if err := c.browser.Connect(); err != nil {
		return errors.Wrap(err, "connect to browser")
	}
	p, err := c.browser.Page(proto.TargetCreateTarget{URL: c.show.URL})
	if err != nil {
		return errors.Wrap(err, "browse page")
	}

	go c.listen(p)

	return nil
}

func (c *Chat) listen(p *rod.Page) {
	for {
		res, err := p.Search("MessageClusterIncoming")
		if err != nil {
			continue
		}
		elems, err := res.All()
		if err != nil {
			continue
		}
		for _, elem := range elems {
			imgElement, err := elem.Element("img")
			if err != nil {
				continue
			}
			imgSrcPtr, err := imgElement.Attribute("src")
			if err != nil {
				continue
			}
			imgSrcURL, err := url.Parse(*imgSrcPtr)
			if err != nil {
				continue
			}
			q := imgSrcURL.Query()
			q.Del("max_width")
			imgSrcURL.RawQuery = q.Encode()
			nameContainer, err := elem.Element(`div[data-testid="MessageSenderName"]`)
			if err != nil {
				continue
			}
			name, err := nameContainer.Text()
			if err != nil {
				continue
			}
			messageTextContainer, err := elem.Element(`div[data-testid="TextMessage"]`)
			if err != nil {
				continue
			}
			messageText, err := messageTextContainer.Text()
			if err != nil {
				continue
			}
			c.prov.AddMessage(&message.Sender{
				ID:        imgSrcURL.String(),
				Name:      name,
				AvatarURL: imgSrcURL.String(),
			}, time.Now(), messageText)
			_ = elem.Remove()
		}
	}
}
