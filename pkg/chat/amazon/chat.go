package amazon

import (
	"github.com/go-rod/rod"
	"github.com/pkg/errors"
	"gitlab.com/frances-fam/chat-widget/pkg/message"
	"gitlab.com/frances-fam/chat-widget/pkg/message/provider"
	"gitlab.com/frances-fam/chat-widget/pkg/show"
)

type Chat struct {
	prov    *provider.SingleSource
	show    *show.Show
	browser *rod.Browser
}

func NewChat(show *show.Show) (*Chat, error) {
	c := &Chat{
		prov: provider.NewSingleSource(&message.Source{
			ID:        "amazon-live",
			Name:      "Amazon Live",
			AvatarURL: "/avatars/amazon.jpg",
		}),
		browser: rod.New(),
		show:    show,
	}
	if err := c.startListening(); err != nil {
		return nil, errors.Wrap(err, "start listening")
	}

	return c, nil
}

func (c *Chat) Provider() provider.Provider {
	return c.prov
}

func (c *Chat) Close() error {
	if c.browser != nil {
		if err := c.browser.Close(); err != nil {
			return errors.Wrap(err, "close browser")
		}
	}

	return nil
}
