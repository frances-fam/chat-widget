package main

import (
	"gitlab.com/frances-fam/chat-widget/internal/app/chat-widget/application"
)

func main() {
	app := application.New()

	app.Run()
}
