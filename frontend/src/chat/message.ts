import type {DateTime} from "luxon";
import type {ISource} from "./source";
import type {ISender} from "./sender";

export interface IMessage {
    ID: String
    Source: ISource
    Sender: ISender
    Timestamp: DateTime
    Payload: string
}


