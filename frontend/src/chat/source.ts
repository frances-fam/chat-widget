export interface ISource {
    ID: string
    Name: string
    AvatarURL: string
}
