export interface ISender {
    ID: string
    Name: string
    AvatarURL: string
}
